namespace MvcAuth.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Contact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Contact", c => c.Long());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Contact");
        }
    }
}
